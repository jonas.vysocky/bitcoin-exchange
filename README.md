# bitcoin-exchange

## Installation

1. Clone repository

   ```bash
   git clone git@gitlab.com:jonas.vysocky/bitcoin-exchange.git
   cd bitcoin-exchange
   ```

2. Get .env

    ```bash
   # You need to have public/.key (ask project owner for it) then:
   make reveal
    ```

3. Prepare project and run it

   ```bash
   make initialization:
   ```
   
4. [Open project in the browser](http://localhost:8910)

### Run NPM in terminal

```bash
docker compose run --rm node bash
```

## Start &times; Stop project

   ```bash
   docker-compose up
   ```

Now the app should be running on http://localhost:8910. Port can be changed in `.env` file (variable NGINX_PORT).

You can stop the project by executing:

   ```bash
   docker-compose down -v
   ```