<?php

namespace App\Command;

use App\Entity\ExchangeRate;
use App\Service\ExchangeRateImporterInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

#[AsCommand(
    name: 'import:exchange-rate:bitcoin',
    description: 'Import actual exchange rate from Bitcoin to EUR/USD into the database',
)]
class ImportBitcoinExchangeRateCommand extends Command
{
    public function __construct(
        private readonly ExchangeRateImporterInterface $exchangeRateImporter
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    /**
     * Command that get json with Bitcoin exchange rate to USD/EUR and store the data in database
     *
     * @param  InputInterface  $input
     * @param  OutputInterface  $output
     * @return int
     * @throws ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->note(sprintf('Starting command: %s', $this->getName()));

        $success = $this->exchangeRateImporter->importExchangeRate();

        if ($success instanceof ExchangeRate) {
            $io->success(sprintf('New exchange rate for bitcoin has been stored - update time "%s"',
                ($success->getCreatedAt())->format('Y-m-d H:i:s T')));
        } else {
            $io->info('Exchange rate is still the same');
        }

        return Command::SUCCESS;
    }
}
