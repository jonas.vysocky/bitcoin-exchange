<?php

namespace App\Entity;

use App\Entity\Base\AbstractBase;
use App\Repository\ExchangeRateRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExchangeRateRepository::class)]
class ExchangeRate extends AbstractBase
{
    #[ORM\Column(type: Types::STRING, length: 255)]
    private string $currency = '';

    #[ORM\Column(type: Types::FLOAT)]
    private float $usdRate = 0;

    #[ORM\Column(type: Types::FLOAT)]
    private float $eurRate = 0;

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): static
    {
        $this->currency = $currency;

        return $this;
    }

    public function getUsdRate(): ?float
    {
        return $this->usdRate;
    }

    public function setUsdRate(float $usdRate): static
    {
        $this->usdRate = $usdRate;

        return $this;
    }

    public function getEurRate(): ?float
    {
        return $this->eurRate;
    }

    public function setEurRate(float $eurRate): static
    {
        $this->eurRate = $eurRate;

        return $this;
    }
}
