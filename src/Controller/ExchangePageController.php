<?php

namespace App\Controller;

use App\Repository\ExchangeRateRepository;
use App\Service\ExchangeRateImporterInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

#[Route('/exchange', name: 'exchange_')]
class ExchangePageController extends AbstractController
{
    public function __construct(
        private readonly ExchangeRateRepository $exchangeRateRepository,
        private readonly ExchangeRateImporterInterface $exchangeRateImporter
    ) {
    }

    /** @throws NonUniqueResultException|TransportExceptionInterface|Exception */
    #[Route('/bitcoin', name: 'bitcoin')]
    public function bitcoinExchangePage(): Response
    {
        $bitcoin = $this->exchangeRateRepository->findLatestBitcoinEntry();
        $success = $this->exchangeRateImporter->importExchangeRate();

        return $this->render('exchange/index.html.twig', [
            'title' => 'Bitcoin',
            'bitcoin' => $bitcoin,
            'updated' => (bool) $success
        ]);
    }
}
