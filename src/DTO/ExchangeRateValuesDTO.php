<?php

declare(strict_types=1);

namespace App\DTO;

use DateTimeImmutable;
use DateTimeZone;
use Exception;

final class ExchangeRateValuesDTO
{
    public string $currency;
    public DateTimeImmutable $createdAt;
    public float $usdRate;
    public float $eurRate;

    /**
     * @param  array  $rateData
     * @return ExchangeRateValuesDTO
     * @throws Exception
     */
    public function toDTO(array $rateData): ExchangeRateValuesDTO
    {
        $createdAt = new DateTimeImmutable($rateData['time']['updatedISO'], new DateTimeZone('UTC'));;

        $dto = new self();
        $dto->currency = $rateData['chartName'];
        $dto->usdRate = $rateData['bpi']['USD']['rate_float'];
        $dto->eurRate = $rateData['bpi']['EUR']['rate_float'];
        $dto->createdAt = $createdAt;

        return $dto;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUsdRate(): float
    {
        return $this->usdRate;
    }

    public function getEurRate(): float
    {
        return $this->eurRate;
    }
}