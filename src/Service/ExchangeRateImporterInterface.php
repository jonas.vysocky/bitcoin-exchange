<?php

declare(strict_types=1);

namespace App\Service;

use App\DTO\ExchangeRateValuesDTO;
use App\Entity\ExchangeRate;
use App\Repository\ExchangeRateRepository;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

interface ExchangeRateImporterInterface
{
    /**
     * Method that request data from Coindesk API in JSON format and then transform them to assoc array
     *
     * @return array
     * @throws TransportExceptionInterface|ServerExceptionInterface|RedirectionExceptionInterface|ClientExceptionInterface
     */
    public function requestCoindeskData(): array;

    /**
     * Check if previous exchange rate does not match current data from API by comparing create dates
     *
     * @param  ExchangeRateValuesDTO  $dto
     * @return ExchangeRate|bool Depends on if new exchange rate has been created
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function saveActualExchangeRate(ExchangeRateValuesDTO $dto): ExchangeRate|bool;

    /**
     * Go through the process of request data from API, transform them to assoc array and store values in DB
     *
     * @return ExchangeRate|bool Depends on if new exchange rate has been created
     * @throws TransportExceptionInterface|ServerExceptionInterface|RedirectionExceptionInterface|ClientExceptionInterface|Exception
     */
    public function importExchangeRate(): ExchangeRate|bool;
}