<?php

declare(strict_types=1);

namespace App\Service;

use App\DTO\ExchangeRateValuesDTO;
use App\Entity\ExchangeRate;
use App\Repository\ExchangeRateRepository;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class ExchangeRateImporterService implements ExchangeRateImporterInterface
{
    public function __construct(
        private readonly string $coindeskApi,
        private readonly HttpClientInterface $client,
        private readonly ExchangeRateValuesDTO $exchangeRateData,
        private readonly ExchangeRateRepository $exchangeRateRepository
    ) {
    }

    public function requestCoindeskData(): array
    {
        $response = $this->client->request('GET', $this->coindeskApi);
        return (array) json_decode($response->getContent(), true) ?: [];
    }

    public function saveActualExchangeRate(ExchangeRateValuesDTO $dto): ExchangeRate|bool
    {
        $latestExchangeRate = $this->exchangeRateRepository->findLatestBitcoinEntry();

        if ($latestExchangeRate === null || ($latestExchangeRate->getCreatedAt()->getTimestamp() !== $dto->getCreatedAt()->getTimestamp())) {
            $exchangeRate = new ExchangeRate();
            $exchangeRate
                ->setCurrency($dto->getCurrency())
                ->setUsdRate($dto->getUsdRate())
                ->setEurRate($dto->getEurRate())
                ->setCreatedAt($dto->getCreatedAt());

            $this->exchangeRateRepository->save($exchangeRate);

            return $exchangeRate;
        }

        return false;
    }

    public function importExchangeRate(): ExchangeRate|bool
    {
        $coindeskData = $this->requestCoindeskData();

        $dto = $this->exchangeRateData->toDTO($coindeskData);

        return $this->saveActualExchangeRate($dto);
    }
}