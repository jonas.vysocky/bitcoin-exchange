#!/bin/bash

# Install new crontab task
crontab bitcoin-exchange-rate-import.txt

# Remove unnecessary txt file
rm bitcoin-exchange-rate-import.txt

# Run cron daemon
cron -f